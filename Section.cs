﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniFileLib
{
	public class Section
	{
		private Dictionary<string, object> data;

		public string Name { get; private set; }

		public Section(string name)
		{
			Name = name;

			data = new Dictionary<string, object>();
		}

		public void Set<T>(string key, T value)
		{
			if (data.ContainsKey(key))
				data[key] = value;
			else
				data.Add(key, value);
		}

		public Dictionary<string, T> GetAll<T>()
		{
			Dictionary<string, T> ret = new Dictionary<string, T>(data.Count);
			try
			{
				foreach(KeyValuePair<string, object> kv in data)
					ret.Add(kv.Key, (T)Convert.ChangeType(kv.Value, typeof(T)));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Can't get convert data to " + typeof(T) + " (" + ex.Message + ")");
			}
			return ret;
		}

		public T Get<T>(string key)
		{
			T ret = default(T);
			try
			{
				if (data.ContainsKey(key))
					ret = (T)Convert.ChangeType(data[key], typeof(T));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Can't get " + data[key] + " as " + typeof(T) + " (" + ex.Message + ")");
			}
			return ret;
		}

		public T Get<T>(string key, T defaultValue)
		{
			T ret = defaultValue;
			try
			{
				if (data.ContainsKey(key))
					ret = (T)Convert.ChangeType(data[key], typeof(T));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Can't get " + data[key] + " as " + typeof(T) + " (" + ex.Message + ")");
			}
			return ret;
		}

		public bool Contains(string key)
		{
			return data.ContainsKey(key);
		}

		public void Delete(string key)
		{
			if (data.ContainsKey(key))
				data.Remove(key);
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("[" + Name + "]").AppendLine();

			foreach (KeyValuePair<string, object> kv in data)
				builder.Append(kv.Key).Append("=").Append(kv.Value).AppendLine();

			return builder.ToString();
		}
	}
}
