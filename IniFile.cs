﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IniFileLib
{
	public enum IniFileMode
	{
		Normal,
		Cached
	}
	public class IniFile
	{
		private List<Section> sections;
		public string Path { get; private set; }

		public IniFile(string path)
		{
			if (string.IsNullOrEmpty(path))
				throw new ArgumentException("Path can't be empty.");

			Path = path;
			sections = new List<Section>();

			Load();
		}

		public void Write<T>(string section, string key, T value)
		{
			if (string.IsNullOrEmpty(section))
				throw new ArgumentException("Section can't be empty.");
			if (string.IsNullOrEmpty(key))
				throw new ArgumentException("Key can't be empty.");

			bool found = false;
			foreach(Section s in sections)
			{
				if (s.Name == section)
				{
					s.Set<T>(key, value);
					found = true;
				}
			}

			if(!found)
			{
				Section s = new Section(section);
				s.Set<T>(key, value);

				sections.Add(s);
			}

			Save();
		}

		public Dictionary<string, T> ReadSection<T>(string section)
		{
			foreach(Section s in sections)
			{
				if(s.Name == section)
				{
					return s.GetAll<T>();
				}
			}
			return new Dictionary<string, T>();
		}

		public T Read<T>(string section, string key)
		{
			T ret = default(T);
			foreach (Section s in sections)
			{
				if (s.Name == section)
				{
					ret = s.Get<T>(key);
				}
			}

			return ret;
		}

		public T Read<T>(string section, string key, T defaultValue)
		{
			T ret = defaultValue;
			foreach (Section s in sections)
			{
				if (s.Name == section)
				{
					ret = s.Get<T>(key, defaultValue);
				}
			}

			return ret;
		}

		public bool ContainSection(string section)
		{
			foreach (Section s in sections)
				if (s.Name == section)
					return true;

			return false;
		}

		public bool ContainsKey(string section, string key)
		{
			foreach (Section s in sections)
			{
				if (s.Name == section)
				{
					return s.Contains(key);
				}
			}

			return false;
		}

		public void Delete(string section)
		{
			bool found = false;

			for (int i = 0; i < sections.Count; i++)
			{
				if (sections[i].Name == section)
				{
					sections.RemoveAt(i);
					found = true;
					break;
				}
			}

			if (found)
				Save();
		}

		public void Delete(string section, string key)
		{
			bool found = false;

			for (int i = 0; i < sections.Count; i++)
			{
				if (sections[i].Name == section)
				{
					sections[i].Delete(key);
					found = true;
					break;
				}
			}

			if (found)
				Save();
		}

		private void Save()
		{
			CheckPath();

			try
			{
				using (StreamWriter writer = new StreamWriter(File.OpenWrite(Path)))
				{
					foreach (Section s in sections)
					{
						writer.Write(s);
						writer.Write("\r\n");
					}
				}
			}
			catch (UnauthorizedAccessException)
			{
				MessageBox.Show("Could not write file (" + Path + ")\r\nPlease restart the program with admin rights.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch(Exception ex)
			{
				MessageBox.Show("Could not write file (" + Path + ") - \r\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void Load()
		{
			int lineCount = 0;
			Section currentSection = null;

			CheckPath();

			sections.Clear();

			try
			{
				using (StreamReader reader = new StreamReader(File.OpenRead(Path)))
				{
					while (!reader.EndOfStream)
					{
						string line = reader.ReadLine().Trim();
						lineCount++;

						if (line.StartsWith(";") || string.IsNullOrEmpty(line)) //line is comment or empty
							continue;
						else if (line.StartsWith("[")) //Line is a new section
						{
							if (!line.EndsWith("]"))
								throw new Exception("Error parsing ini file: (" + lineCount + ") " + line);

							string name = line.Substring(1, line.Length - 2);
							currentSection = new Section(name);

							sections.Add(currentSection);
						}
						else
						{
							if (!line.Contains("="))
								throw new Exception("Error parsing ini file: (" + lineCount + ") " + line);

							int equalSign = line.IndexOf("=");
							string key = line.Substring(0, equalSign).Trim();
							string value = line.Substring(equalSign + 1).Trim();

							if (currentSection != null)
								currentSection.Set(key, value);
						}
					}
				}
			}
			catch
			{
				File.Create(Path).Close();
			}
		}

		private void CheckPath()
		{
			string path = System.IO.Path.GetFullPath(Path);
			path = System.IO.Path.GetDirectoryName(path);

			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);
		}
	}
}
